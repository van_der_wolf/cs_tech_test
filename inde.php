<?php

class Combinator
{
    private $vector;

    private $max;

    private $result = [];

    function __construct($vector, $max)
    {
        $this->vector = $vector;
        $this->max = $max;
    }

    function combine()
    {
        return $this->_combine();
    }

    private function _combine()
    {
        $counter = $this->getCounter();
        $iterate = TRUE;
        while ($iterate) {
            $tmp = [];
            for ($i = 0; $i < count($counter); $i++) {
                $tmp[] = $this->vector[$counter[$i]];
            }
            $this->result[] = $tmp;
            $this->increment($counter);
            if ($counter[0] > count($this->vector) - $this->max) {
                $iterate = FALSE;
            }
        }
        return $this->result;
    }

    private function increment(&$counter) {
        for ($i = count($counter) - 1; $i >= 0; $i--) {
            if ($counter[$i] < count($this->vector) - 1) {
                $counter[$i]++;
                // If $i is not the last element
                if ($i < count($counter) - 1) {
                    for ($j = $i; $j < count($counter) - 1; $j++) {
                        if ($counter[$j] < count($this->vector) - 1) {
                            $counter[$j + 1] = $counter[$j] + 1;
                        }
                    }
                    if ($counter[$i] > count($this->vector) - ($this->max - $i)) {
                        continue;
                    }
                }
                break;
            }
        }
    }

    private function getCounter() {
        $counter = [];
        for ($i = 0; $i < $this->max; $i++) {
            $counter[] = $i;
        }
        return $counter;
    }
}

$vector = [0, 1, 2, 3, 4, 5, 6];
$combinator = new Combinator($vector, 4);

$result = $combinator->combine();

foreach ($result as $item) {
    print implode(', ', $item) . "\n\r";
}